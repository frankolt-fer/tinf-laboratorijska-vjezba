#include <math.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int Rows, Columns;
    double **Data;
} Matrix;

Matrix CreateMatrix(const int Rows, const int Columns);
void DestroyMatrix(Matrix m);

int InputMatrixDimension(const char *Prompt);
Matrix InputMatrix(const char *Prompt, int Rows, int Columns);
void OutputEmptyLine();
void OutputMatrix(const char *Message, Matrix m);

double lb(double x);
Matrix CalculateInputSymbolProbabilities(Matrix JointProbabilities);
Matrix CalculateOutputSymbolProbabilities(Matrix JointProbabilities);
double CalcualteEntropy(Matrix Probabilities);
double CalculateNoiseEntropy(Matrix JointProbabilities, Matrix InputSymbolProbabilities);
double CalculateEquivocation(Matrix JointProbabilities, Matrix OutputSymbolProbabilities);
double CalculateMutualInformation(Matrix JointProbabilities, Matrix InputSymbolProbabilities, Matrix OutputSymbolProbabilities);

Matrix CreateMatrix(const int Rows, const int Columns)
{
    double **Data = malloc(Rows * sizeof *Data);
    if (Data == NULL) {
        return (Matrix) {0, 0, NULL};
    }
    for (int i = 0; i < Rows; ++i) {
        Data[i] = malloc(Columns * sizeof **Data);
    }
    return (Matrix) {Rows, Columns, Data};
}

void DestroyMatrix(Matrix m)
{
    for (int i = 0; i < m.Rows; ++i) {
        free(m.Data[i]);
    }
    free(m.Data);
}

int InputMatrixDimension(const char *Prompt)
{
    int Dimension;
    puts(Prompt);
    (void) scanf("%d", &Dimension);
    return Dimension;
}

Matrix InputMatrix(const char *Prompt, int Rows, int Columns)
{
    Matrix m = CreateMatrix(Rows, Columns);
    puts(Prompt);
    for (int i = 0; i < m.Rows; ++i) {
        for (int j = 0; j < m.Columns; ++j) {
            (void) scanf("%lf", m.Data[i] + j);
        }
    }
    return m;
}

void OutputEmptyLine()
{
    puts("");
}

void OutputMatrix(const char *Message, Matrix m)
{
    puts(Message);
    for (int i = 0; i < m.Rows; ++i) {
        printf("%f", m.Data[i][0]);
        for (int j = 1; j < m.Columns; ++j) {
            printf(" %f", m.Data[i][j]);
        }
        OutputEmptyLine();
    }
}

double lb(double x)
{
    return log(x) / log(2);
}

Matrix CalculateInputSymbolProbabilities(Matrix JointProbabilities)
{
    Matrix m = CreateMatrix(1, JointProbabilities.Rows);
    for (int i = 0; i < JointProbabilities.Rows; ++i) {
        double Probability = 0;
        for (int j = 0; j < JointProbabilities.Columns; ++j) {
            Probability += JointProbabilities.Data[i][j];
        }
        m.Data[0][i] = Probability;
    }
    return m;
}

Matrix CalculateOutputSymbolProbabilities(Matrix JointProbabilities)
{
    Matrix m = CreateMatrix(1, JointProbabilities.Columns);
    for (int i = 0; i < JointProbabilities.Columns; ++i) {
        double Probability = 0;
        for (int j = 0; j < JointProbabilities.Rows; ++j) {
            Probability += JointProbabilities.Data[j][i];
        }
        m.Data[0][i] = Probability;
    }
    return m;
}

double CalcualteEntropy(Matrix Probabilities)
{
    double Sum = 0;
    for (int i = 0; i < Probabilities.Rows; ++i) {
        for (int j = 0; j < Probabilities.Columns; ++j) {
            double Probability = Probabilities.Data[i][j];
            if (Probability != 0) {
                Sum += Probability * lb(Probability);
            }
        }
    }
    return -Sum;
}

double CalculateNoiseEntropy(Matrix JointProbabilities, Matrix InputSymbolProbabilities)
{
    double Sum = 0;
    for (int i = 0; i < JointProbabilities.Rows; ++i) {
        double InputSymbolProbability = InputSymbolProbabilities.Data[0][i];
        for (int j = 0; j < JointProbabilities.Columns; ++j) {
            double JointProbability = JointProbabilities.Data[i][j];
            if (JointProbability != 0 && InputSymbolProbability != 0) {
                Sum += JointProbability * lb(JointProbability / InputSymbolProbability);
            }
        }
    }
    return -Sum;
}

double CalculateEquivocation(Matrix JointProbabilities, Matrix OutputSymbolProbabilities)
{
    double Sum = 0;
    for (int i = 0; i < JointProbabilities.Rows; ++i) {
        for (int j = 0; j < JointProbabilities.Columns; ++j) {
            double JointProbability = JointProbabilities.Data[i][j];
            double OutputSymbolProbability = OutputSymbolProbabilities.Data[0][j];
            if (JointProbability != 0 && OutputSymbolProbability != 0) {
                Sum += JointProbability * lb(JointProbability / OutputSymbolProbability);
            }
        }
    }
    return -Sum;
}

double CalculateMutualInformation(Matrix JointProbabilities, Matrix InputSymbolProbabilities, Matrix OutputSymbolProbabilities)
{
    double Sum = 0;
    for (int i = 0; i < JointProbabilities.Rows; ++i) {
        double InputSymbolProbability = InputSymbolProbabilities.Data[0][i];
        for (int j = 0; j < JointProbabilities.Columns; ++j) {
            double JointProbability = JointProbabilities.Data[i][j];
            double OutputSymbolProbability = OutputSymbolProbabilities.Data[0][j];
            if (JointProbability != 0 && InputSymbolProbability != 0 && OutputSymbolProbability != 0) {
                Sum += JointProbability * lb(JointProbability / (InputSymbolProbability * OutputSymbolProbability));
            }
        }
    }
    return Sum;
}

int main(void)
{
    int Rows = InputMatrixDimension("Unesite broj redova matrice:");
    int Columns = InputMatrixDimension("Unesite broj stupaca matrice:");
    Matrix JointProbabilities = InputMatrix("Unesite vrijednosti matrice:", Rows, Columns);
    Matrix InputSymbolProbabilities = CalculateInputSymbolProbabilities(JointProbabilities);
    Matrix OutputSymbolProbabilities = CalculateOutputSymbolProbabilities(JointProbabilities);
    double InputEntropy = CalcualteEntropy(InputSymbolProbabilities);
    double OutputEntropy = CalcualteEntropy(OutputSymbolProbabilities);
    double JointEntropy = CalcualteEntropy(JointProbabilities);
    double NoiseEntropy = CalculateNoiseEntropy(JointProbabilities, InputSymbolProbabilities);
    double Equivocation = CalculateEquivocation(JointProbabilities, OutputSymbolProbabilities);
    double MutualInformation = CalculateMutualInformation(JointProbabilities, InputSymbolProbabilities, OutputSymbolProbabilities);

    OutputEmptyLine();
    OutputMatrix("Vjerojatnosti pojavljivanja simbola na ulazu kanala:", InputSymbolProbabilities);
    OutputEmptyLine();
    OutputMatrix("Vjerojatnosti pojavljivanja simbola na izlazu kanala:", OutputSymbolProbabilities);
    OutputEmptyLine();
    printf("Entropija ulaznog skupa simbola:\n%f\n\n", InputEntropy);
    printf("Entropija izlaznog skupa simbola:\n%f\n\n", OutputEntropy);
    printf("Združena entropija:\n%f\n\n", JointEntropy);
    printf("Entropija šuma:\n%f\n\n", NoiseEntropy);
    printf("Ekvivokacija:\n%f\n\n", Equivocation);
    printf("Srednji uzajamni sadržaj informacije:\n%f\n", MutualInformation);

    DestroyMatrix(InputSymbolProbabilities);
    DestroyMatrix(OutputSymbolProbabilities);
    DestroyMatrix(JointProbabilities);
}
